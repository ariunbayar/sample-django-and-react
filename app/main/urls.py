from django.urls import path
import frontend.views
import user.views

urlpatterns = [
    path('', frontend.views.index),
    path('get_users/', user.views.get_users),
    path('get_detail/<str:name>/', user.views.get_detail),
    path('preload/', frontend.views.preload),
    path('preload/get-image/', frontend.views.get_image),
]
