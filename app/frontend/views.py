import random
import os
import time
from django.shortcuts import render
from django.http import HttpResponse
from django.conf import settings


def index(request):
    return render(request, 'frontend/index.html', {})


def preload(request):
    return render(request, 'frontend/preload.html', {})

def get_image(request):
    n = random.randint(500, 3000)
    time.sleep(n / 1000)
    filename = os.path.join(settings.BASE_DIR, 'static', 'image.png')
    fsock = open(filename, 'rb')
    return HttpResponse(fsock, content_type='image/png')
