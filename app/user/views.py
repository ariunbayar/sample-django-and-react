import random

from django.shortcuts import render
from django.http import JsonResponse


def get_users(request):

    import time
    time.sleep(2)

    users = [
        'James',
        'Jordan',
        'Jake',
        'Jasmine'
    ]

    context = {
        'users': users
    }

    return JsonResponse(context)


def get_detail(request, name):

    import time
    time.sleep(3)

    context = {
        'name': name,
        'age': random.randint(18, 35),
    }

    return JsonResponse(context)
