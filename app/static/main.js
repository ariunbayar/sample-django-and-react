'use strict'


class ActivityType extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            current_choice: 0,
            choices: ['', 'aa', 'bbb', 'd'],
            text: '',
        }
    this.changeChoice = this.changeChoice.bind(this)
    }

    changeChoice(event) {
        this.setState({
            current_choice: parseInt(event.target.value),
        })
    }

    render() {

        let items = []

        this.state.choices.forEach((value, index) => {
            items.push(
                <option value={ index } key={ index }>{ value }</option>
            )
        })

        return (
            <div>
                <div>
                    <select value={ this.state.current_choice } onChange={ this.changeChoice }> { items } </select>
                </div>
                {this.state.current_choice == 0 &&
                    <div>
                        <input value={ this.state.value }/>
                    </div>
                }
            </div>
        )

    }
}


class User extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            name: props.name,
            age: null,
            is_loading: false,
        }

        this.loadDetail = this.loadDetail.bind(this)
    }

    async loadDetail() {
        this.setState({
            is_loading: true,
        })
        let rsp = await axios.get('/get_detail/' + this.state.name + '/')
        this.setState({
            name: rsp.data.name,
            age: rsp.data.age,
            is_loading: false,
        })
    }

    render() {

        if (this.state.is_loading) {

            return (
                <span> Loading user details ... </span>
            )

        } else {

            if (this.state.age) {

                return (
                    <li>
                        <strong>Name</strong>: { this.state.name }<br/>
                        <strong>Age</strong>: { this.state.age }
                    </li>
                )

            } else {

                return (
                    <li>
                        <button onClick={ this.loadDetail }>Show Detail</button>
                        { this.state.name }
                    </li>
                )

            }

        }

    }
}


class App extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            names: [],
            is_loading: false,
        }
        this.loadUsers = this.loadUsers.bind(this)
    }

    async loadUsers() {
        this.setState({is_loading: true})

        let rsp = await axios.get('/get_users/')
        this.setState({
            names: rsp.data.users,
            is_loading: false,
        })
    }

    render() {

        if (this.state.is_loading) {
            return (
                <span>Loading users ...</span>
            )

        } else {

            let items = []

            this.state.names.forEach((name, index) => {
                items.push(
                    <User name={ name } key={ index }/>
                )
            })

            return (
                <div>
                    <ul>
                        { items }
                        <li>
                                <button onClick={ this.loadUsers }>Load users</button>
                        </li>
                    </ul>
                    <ActivityType/>
                </div>
            )

        }

    }
}

ReactDOM.render(
    <App/>,
    document.querySelector('#react-app')
)
